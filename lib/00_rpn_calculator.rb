class RPNCalculator
  attr_accessor :num_stack

  def initialize
    @num_stack = []
  end

  def push(num)
    @num_stack << num.to_f
  end

  def plus
    check_calc
    @num_stack << num_stack[-2..-1].reduce(:+)
    clear_operated_nums
  end

  def minus
    check_calc
    @num_stack << num_stack[-2..-1].reduce(:-)
    clear_operated_nums
  end

  def times
    check_calc
    @num_stack << num_stack[-2..-1].reduce(:*)
    clear_operated_nums
  end

  def divide
    check_calc
    @num_stack << num_stack[-2..-1].reduce(:/)
    clear_operated_nums
  end

  def value
    @num_stack[-1]
  end

  def clear_operated_nums
    @num_stack.reverse!
    @num_stack.delete_at(1)
    @num_stack.delete_at(1)
    @num_stack.reverse!
  end

  def check_calc
    raise "calculator is empty" if @num_stack == []
  end

end




## Target completion date: May 12, 2017 - EOD
